using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Repository.RMenus
{
public class MenusRepository : GenericRepository<Menus>, IMenusRepository
{
public MenusRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}