using grishmaRestaurant.Repository.Domain;

namespace grishmaRestaurant.Repository.RMenus
{
public interface IMenusRepository : IGenericRepository<Menus>
{
}
}