using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Repository.RCustomer
{
public interface ICustomerRepository : IGenericRepository<Customer>
{
}
}