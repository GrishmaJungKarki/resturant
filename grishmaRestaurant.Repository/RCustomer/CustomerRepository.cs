using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Repository.RCustomer
{
public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
{
public CustomerRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}