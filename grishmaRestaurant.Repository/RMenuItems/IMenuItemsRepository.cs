using grishmaRestaurant.Repository.Domain;

namespace grishmaRestaurant.Repository.RMenuItems
{
public interface IMenuItemsRepository : IGenericRepository<MenuItems>
{
}
}