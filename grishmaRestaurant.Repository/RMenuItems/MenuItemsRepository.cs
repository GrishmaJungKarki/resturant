using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Repository.RMenuItems
{
public class MenuItemsRepository : GenericRepository<MenuItems>, IMenuItemsRepository
{
public MenuItemsRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}