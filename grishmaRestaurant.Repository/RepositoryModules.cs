using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using grishmaRestaurant.Repository.RCustomer;
using grishmaRestaurant.Repository.RMenus;
using grishmaRestaurant.Repository.RMenuItems;
using grishmaRestaurant.Repository.RStaffRoles;
using grishmaRestaurant.Repository.RStaff;
using grishmaRestaurant.Repository.RMeals;
using grishmaRestaurant.Repository.RMealDishes;
namespace grishmaRestaurant.Repository
{
public static class RepositoryModule
{
public static void Register(IServiceCollection services, string connection,
string migrationsAssembly)
{
    services.AddDbContext<RestaurantContext>(options =>
    options.UseSqlServer(connection, builder =>
    builder.MigrationsAssembly(migrationsAssembly)));
    
    services.AddTransient<ICustomerRepository, CustomerRepository>();
    services.AddTransient<IMenusRepository, MenusRepository>();
    services.AddTransient<IMenuItemsRepository, MenuItemsRepository>();
    services.AddTransient<IMealsRepository, MealsRepository>();
    services.AddTransient<IStaffRepository, StaffRepository>();
    services.AddTransient<IStaffRolesRepository, StaffRolesRepository>();
    services.AddTransient<IMealDishesRepository, MealDishesRepository>();
    
}
}
}