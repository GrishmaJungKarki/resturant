using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Repository.RMealDishes
{
public class MealDishesRepository : GenericRepository<MealDishes>, IMealDishesRepository
{
public MealDishesRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}