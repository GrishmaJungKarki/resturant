using grishmaRestaurant.Repository.Domain;

namespace grishmaRestaurant.Repository.RMealDishes
{
public interface IMealDishesRepository : IGenericRepository<MealDishes>
{
}
}