using grishmaRestaurant.Repository.Domain;

namespace grishmaRestaurant.Repository.RStaffRoles
{
public interface IStaffRolesRepository : IGenericRepository<StaffRoles>
{
}
}