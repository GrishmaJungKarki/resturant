using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Repository.RStaffRoles
{
public class StaffRolesRepository : GenericRepository<StaffRoles>, IStaffRolesRepository
{
public StaffRolesRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}