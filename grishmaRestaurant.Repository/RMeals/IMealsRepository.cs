using grishmaRestaurant.Repository.Domain;

namespace grishmaRestaurant.Repository.RMeals
{
public interface IMealsRepository : IGenericRepository<Meals>
{
}
}