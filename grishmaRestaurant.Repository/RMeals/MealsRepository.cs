using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Repository.RMeals
{
public class MealsRepository : GenericRepository<Meals>, IMealsRepository
{
public MealsRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}