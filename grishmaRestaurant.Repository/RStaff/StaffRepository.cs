using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Repository.RStaff
{
public class StaffRepository : GenericRepository<Staff>, IStaffRepository
{
public StaffRepository(RestaurantContext dbContext)
{
DbContext = dbContext;
}
}
}