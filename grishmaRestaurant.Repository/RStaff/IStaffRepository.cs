using grishmaRestaurant.Repository.Domain;

namespace grishmaRestaurant.Repository.RStaff
{
public interface IStaffRepository : IGenericRepository<Staff>
{
}
}