using System;
namespace grishmaRestaurant.Repository.Domain
{
public class MenuItems
{
    public int id {get; set;}

    public string menu_item_name {get; set;}

    public string other_details {get; set; }

    public virtual Menus Menus {get; set; }
}
}    