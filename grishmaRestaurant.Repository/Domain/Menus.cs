using System;
namespace grishmaRestaurant.Repository.Domain
{
public class Menus
{
    public int id {get; set;}

    public string menu_name {get; set;}

    public DateTime available_date_from {get; set;}
    public DateTime available_date_to {get; set;}

    public string other_deals {get; set;}

}
}