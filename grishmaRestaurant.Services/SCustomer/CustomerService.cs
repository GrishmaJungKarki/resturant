using grishmaRestaurant.Repository.Domain;
using grishmaRestaurant.Repository.RCustomer;
namespace grishmaRestaurant.Services.SCustomer
{
public class CustomerService : GenericService<Customer>, ICustomerService
{
public CustomerService(ICustomerRepository customerRepository) :
base(customerRepository)
{
}
}
}