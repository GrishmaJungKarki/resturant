using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Services.SCustomer
{
public interface ICustomerService : IGenericService<Customer>
{
}
}