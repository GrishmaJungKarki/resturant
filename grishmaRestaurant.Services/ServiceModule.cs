using Microsoft.Extensions.DependencyInjection;
using grishmaRestaurant.Services.SCustomer;
using grishmaRestaurant.Services.SMealDishes;
using grishmaRestaurant.Services.SMeals;
using grishmaRestaurant.Services.SMenuItems;
using grishmaRestaurant.Services.SMenus;
using grishmaRestaurant.Services.SStaff;
using grishmaRestaurant.Services.SStaffRoles;

namespace grishmaRestaurant.Services
{
public static class ServicesModule
{
public static void Register(IServiceCollection services)
{
services.AddTransient<ICustomerService, CustomerService>();
services.AddTransient<IMenusService, MenusService>();
services.AddTransient<IMenuItemsService, MenuItemsService>();
services.AddTransient<IMealsService, MealsService>();
services.AddTransient<IStaffService, StaffService>();
services.AddTransient<IStaffRolesService, StaffRolesService>();
services.AddTransient<IMealDishesService, MealDishesService>();
}
}
}