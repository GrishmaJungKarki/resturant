using grishmaRestaurant.Repository.Domain;
using grishmaRestaurant.Repository.RStaff;
namespace grishmaRestaurant.Services.SStaff
{
public class StaffService : GenericService<Staff>, IStaffService
{
public StaffService(IStaffRepository staffRepository) :
base(staffRepository)
{
}
}
}