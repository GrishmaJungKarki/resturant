using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Services.SStaff
{
public interface IStaffService : IGenericService<Staff>
{
}
}