using grishmaRestaurant.Repository.Domain;
using grishmaRestaurant.Repository.RMeals;
namespace grishmaRestaurant.Services.SMeals
{
public class MealsService : GenericService<Meals>, IMealsService
{
public MealsService(IMealsRepository mealsRepository) :
base(mealsRepository)
{
}
}
}