using grishmaRestaurant.Repository.Domain;
using grishmaRestaurant.Repository.RStaffRoles;
namespace grishmaRestaurant.Services.SStaffRoles
{
public class StaffRolesService : GenericService<StaffRoles>, IStaffRolesService
{
public StaffRolesService(IStaffRolesRepository staffRolesRepository) :
base(staffRolesRepository)
{
}
}
}