using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Services.SStaffRoles
{
public interface IStaffRolesService : IGenericService<StaffRoles>
{
}
}