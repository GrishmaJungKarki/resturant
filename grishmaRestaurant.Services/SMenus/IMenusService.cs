using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Services.SMenus
{
public interface IMenusService : IGenericService<Menus>
{
}
}