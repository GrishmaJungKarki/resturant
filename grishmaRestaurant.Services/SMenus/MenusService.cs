using grishmaRestaurant.Repository.Domain;
using grishmaRestaurant.Repository.RMenus;
namespace grishmaRestaurant.Services.SMenus
{
public class MenusService : GenericService<Menus>, IMenusService
{
public MenusService(IMenusRepository menusRepository) :
base(menusRepository)
{
}
}
}