using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Services.SMealDishes
{
public interface IMealDishesService : IGenericService<MealDishes>
{
}
}