using grishmaRestaurant.Repository.Domain;
using grishmaRestaurant.Repository.RMealDishes;
namespace grishmaRestaurant.Services.SMealDishes
{
public class MealDishesService : GenericService<MealDishes>, IMealDishesService
{
public MealDishesService(IMealDishesRepository mealDishesRepository) :
base(mealDishesRepository)
{
}
}
}