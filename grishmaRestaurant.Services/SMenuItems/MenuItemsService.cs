using grishmaRestaurant.Repository.Domain;
using grishmaRestaurant.Repository.RMenuItems;
namespace grishmaRestaurant.Services.SMenuItems
{
public class MenuItemsService : GenericService<MenuItems>, IMenuItemsService
{
public MenuItemsService(IMenuItemsRepository menuItemsRepository) :
base(menuItemsRepository)
{
}
}
}