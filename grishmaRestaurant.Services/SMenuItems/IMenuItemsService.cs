using grishmaRestaurant.Repository.Domain;
namespace grishmaRestaurant.Services.SMenuItems
{
public interface IMenuItemsService : IGenericService<MenuItems>
{
}
}